FROM aztek/awscli:1.16.124 AS build_stage

LABEL maintainer="robert@aztek.io"

RUN apk update

# hadolint ignore=DL3018
RUN apk add --no-cache \
        ca-certificates \
        curl

COPY ["version.json", "/version.json"]

RUN curl -sLO "https://github.com/kubernetes/kops/releases/download/$(jq -r .KOPS_VERSION /version.json)/kops-linux-amd64"
RUN curl -sLO "https://storage.googleapis.com/kubernetes-release/release/$(jq -r .KUBECTL_VERSION /version.json)/bin/linux/amd64/kubectl"

RUN chmod +x kops-linux-amd64 kubectl

FROM aztek/awscli:1.16.124 AS final

# hadolint ignore=DL3018
RUN apk add --no-cache \
        ca-certificates

COPY --from=build_stage ["kops-linux-amd64", "/usr/local/bin/kops"]
COPY --from=build_stage ["kubectl", "/usr/local/bin/kubectl"]

ENTRYPOINT ["/usr/local/bin/kops", "--help"]

