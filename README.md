# README

## INFO

This was created since I did not find an official Kops image.

You might be thinking: "Why even do this?"

The answer is simple: I wanted something like this so that I could automate standing up a Kubernetes cluster using GitLab.  I want to treat all of my resources like cattle especially my development Kubernetes cluster:

https://gitlab.com/aztek-io/kubernetes_iac

Note: The above repo is not available while waiting for the `sensitve_content` argument in terraform's `local_file` module.

## HUB REPO

Here is the hub.docker.com repo:

https://hub.docker.com/r/aztek/kops/
